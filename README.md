# voxl-mpa-exposure-service

## Building

### Dependencies

* libmodal_pipe


### voxl-cross Build

```
$ opkg update
$ install_build_deps.sh stable
$ ./build.sh 64
```

### Emulator Build

(issues with opencv 32-bit install right now)

```
$ opkg update
$ opkg install libmodal_pipe
$ opkg install opencv
$ ./build.sh native
```

## Run on Target

From PC:
```
$ ./install_on_voxl.sh
```

On Target:

```
$ voxl-mpa-exposure /run/mpa/tracking/
```
