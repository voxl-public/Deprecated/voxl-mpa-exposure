############################################################################## 
#  Copyright 2020 ModalAI Inc.
# 
#  Redistribution and use in source and binary forms, with or without
#  modification, are permitted provided that the following conditions are met:
# 
#  1. Redistributions of source code must retain the above copyright notice,
#     this list of conditions and the following disclaimer.
# 
#  2. Redistributions in binary form must reproduce the above copyright notice,
#     this list of conditions and the following disclaimer in the documentation
#     and/or other materials provided with the distribution.
# 
#  3. Neither the name of the copyright holder nor the names of its contributors
#     may be used to endorse or promote products derived from this software
#     without specific prior written permission.
# 
#  4. The Software is used solely in conjunction with devices provided by
#     ModalAI Inc.
# 
#  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
#  AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
#  IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
#  ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
#  LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
#  CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
#  SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
#  INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
#  CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
#  ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
#  POSSIBILITY OF SUCH DAMAGE.
############################################################################## 

LOCAL_PATH:= $(call my-dir)
ROOT_REL:= ../..
ROOT_ABS:= $(LOCAL_PATH)/../..

include $(CLEAR_VARS)

ROOT_REL:= ../..
ROOT_ABS:= $(LOCAL_PATH)/../..

LOCAL_SRC_FILES := \
  $(ROOT_REL)/src/main.cpp 
  

LOCAL_C_INCLUDES += \
  $(ROOT_ABS)/src \
  $(ROOT_ABS)/../opencv/include \
  $(ROOT_ABS)/../opencv/build/install/sdk/native/jni/include \
  $(ROOT_ABS)/libmodal_pipe/library/include
  

LOCAL_CPPFLAGS := -fexceptions -fopenmp -std=c++11 -frtti -Wno-invalid-partial-specialization -Wno-c++11-narrowing -pthread
LOCAL_CPPFLAGS += -DCERES_USE_CXX11_THREADS

 LOCAL_LDLIBS := -L${ALL_BUILD_DIR}/lib/ -Wl,--start-group -lz -llog -landroid -ldl -lstdc++ -lmodal_pipe -lgflags -lglog -lc++ -lceres -lcpufeatures -lIlmImf -littnotify -llibjasper -llibjpeg-turbo -llibpng -llibprotobuf -llibtiff -llibwebp -llibwebp -ltegra_hal  -lopencv_aruco -lopencv_bgsegm -lopencv_bioinspired -lopencv_calib3d -lopencv_ccalib -lopencv_core -lopencv_datasets -lopencv_dnn -lopencv_dnn_objdetect -lopencv_dpm -lopencv_face -lopencv_features2d -lopencv_flann -lopencv_fuzzy -lopencv_hfs -lopencv_highgui -lopencv_imgcodecs -lopencv_img_hash -lopencv_imgproc -lopencv_line_descriptor -lopencv_ml -lopencv_objdetect -lopencv_optflow -lopencv_phase_unwrapping -lopencv_photo -lopencv_plot -lopencv_reg -lopencv_rgbd -lopencv_saliency -lopencv_shape -lopencv_stereo -lopencv_stitching -lopencv_structured_light -lopencv_superres -lopencv_surface_matching -lopencv_text -lopencv_tracking -lopencv_video -lopencv_videoio -lopencv_videostab -lopencv_xfeatures2d -lopencv_ximgproc -lopencv_xobjdetect -lopencv_xphoto
LOCAL_LDFLAGS := -L${ALL_BUILD_DIR}/lib/ -fopenmp -v

LOCAL_MODULE := voxl-mpa-exposure

include $(BUILD_EXECUTABLE)

#
# Simple command line app: exposure-cli
# 
include $(CLEAR_VARS)

ROOT_REL:= ../..
ROOT_ABS:= $(LOCAL_PATH)/../..

LOCAL_SRC_FILES := \
  $(ROOT_REL)/src/exposure-cli.cpp

LOCAL_C_INCLUDES += \
  $(ROOT_ABS)/src \
  $(ROOT_ABS)/libmodal_pipe/library/include


LOCAL_CPPFLAGS := -fexceptions -std=c++11 -frtti
LOCAL_LDLIBS := -llog -lmodal_pipe -landroid -ldl -lstdc++ -lc++ 
LOCAL_LDFLAGS := -L${ALL_BUILD_DIR}/lib/

LOCAL_MODULE := exposure-cli

include $(BUILD_EXECUTABLE)



