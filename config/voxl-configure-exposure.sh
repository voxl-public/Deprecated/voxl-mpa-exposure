#!/bin/bash
################################################################################
# Copyright 2021 ModalAI Inc.
#
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions are met:
#
# 1. Redistributions of source code must retain the above copyright notice,
#    this list of conditions and the following disclaimer.
#
# 2. Redistributions in binary form must reproduce the above copyright notice,
#    this list of conditions and the following disclaimer in the documentation
#    and/or other materials provided with the distribution.
#
# 3. Neither the name of the copyright holder nor the names of its contributors
#    may be used to endorse or promote products derived from this software
#    without specific prior written permission.
#
# 4. The Software is used solely in conjunction with devices provided by
#    ModalAI Inc.
#
# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
# AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
# IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
# ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
# LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
# CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
# SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
# INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
# CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
# ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
# POSSIBILITY OF SUCH DAMAGE.
################################################################################

CONFIG_FILE=/etc/modalai/voxl-mpa-exposure.conf
USER=$(whoami)


print_usage () {
	echo "General Usage:"
	echo "voxl-configure-exposure"
	echo ""
	echo "To perform factory configuration for VOXL Flight Deck"
	echo "voxl-configure-exposure -f"
	echo ""
	echo "show this help message:"
	echo "voxl-configure-exposure -h"
	echo ""
	exit 0
}


## set most parameters which don't have quotes in json
set_param () {
	if [ "$#" != "2" ]; then
		echo "set_param expected 2 args"
		exit 1
	fi

	# remove quotes if they exist
	var=$1
	var="${var%\"}"
	var="${var#\"}"
	val=$2
	val="${val%\"}"
	val="${val#\"}"

	sed -E -i "/\"$var\":/c\	\"$var\":	$val," ${CONFIG_FILE}
}

## set string parameters which need quotes in json
set_param_string () {
	if [ "$#" != "2" ]; then
		echo "set_param_string expected 2 args"
		exit 1
	fi

	var=$1
	var="${var%\"}"
	var="${var#\"}"

	sed -E -i "/\"$var\":/c\	\"$var\":	\"$2\"," ${CONFIG_FILE}
}

enable_service_and_exit () {
	systemctl daemon-reload
	echo "enabling voxl-exposure-server systemd service"
	systemctl enable voxl-exposure-server.service
	echo "starting voxl-exposure-server systemd service"
	systemctl restart voxl-exposure-server.service
	echo "DONE configuring voxl-exposure-server"
	exit 0
}

## print help message if requested
if [ "$1" == "-h" ]; then
	print_usage
	exit 0
elif [ "$1" == "--help" ]; then
	print_usage
	exit 0
fi

## sanity checks
if [ "${USER}" != "root" ]; then
	echo "Please run this script as root"
	exit 1
fi

## perform factory setup if requested
if [ "$1" == "-f" ]; then
	# delete the current config file and reload defaults
	rm -f ${CONFIG_FILE}
	voxl-exposure-server -c
	echo "disabling voxl-exposure-server"
	systemctl disable voxl-exposure-server
	systemctl stop voxl-exposure-server
	exit 0
fi


# have the voxl-exposure-server binary open and close the config file
# this will clean it up and add any new values from updates
echo "loading and updating config file with voxl-exposure-server -c"
voxl-exposure-server -c


# ask if the user wants the service enabled or not
echo " "
echo "Do you want to enable the voxl-exposure-server service to allow"
echo "Depth-from-stereo processing?"
echo "note that Visual Obstacle Avoidance with PX4 will require further"
echo "configuration of voxl-vision-px4 and PX4 itself."
select opt in "yes" "no"; do
case $opt in
yes )
	echo "enabling service"
	systemctl enable voxl-exposure-server
	echo "starting service"
	systemctl start voxl-exposure-server
	echo "Done enabling voxl-exposure-server"
	exit 0
	break;;
no )
	echo "Disabling voxl-exposure-server service"
	systemctl disable voxl-exposure-server
	systemctl stop voxl-exposure-server
	echo "Done disabling voxl-exposure-server"
	exit 0
	break;;
*)
	echo "invalid option"
	esac
done



