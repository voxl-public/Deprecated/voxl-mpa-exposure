/**
 *  @file exposure-hist.cpp
 *  @brief Main file for running the application
 */
/*******************************************************************************
 * Copyright 2021 ModalAI Inc.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its contributors
 *    may be used to endorse or promote products derived from this software
 *    without specific prior written permission.
 *
 * 4. The Software is used solely in conjunction with devices provided by
 *    ModalAI Inc.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 ******************************************************************************/


// C/C++ Includes
#include <iostream>
#include <signal.h>
#include <cstring>
#include <unistd.h>
#include <stdio.h>
#include <stdlib.h>
#include <atomic>
#include <math.h>
#include <float.h>


// Package dependencies
#include "opencv2/opencv.hpp"

// ModalAI Library Includes
#include "modal_pipe.h"
#include "modal_pipe_interfaces.h"

#include "exposure-hist.h"

/** Data callback for the Camera.  This displays the camera data
 *
 *  @param[in] ch The channel index issuing the callback
 *  @param[in] data A pointer to the data
 *  @param[in] bytes The number of bytes in the data
 */
bool ExposureHist::update_exposure(
	uint8_t*	image_data, 
	int 		width,
	int 		height, 
	uint64_t	cur_exposure_ns, 
	int16_t		cur_gain,
	uint64_t* 	set_exposure_ns, 
	int16_t* 	set_gain )
{
	bool update = false; // set to true if values should be updated

	// TODO: Try UMat for OpenCL

	cv::Mat image = cv::Mat(height, width, CV_8U, image_data).clone();

	static float err_i;
	int histSize = 256;
	float range[] = { 0, 256 }; //the upper boundary is exclusive
	const float* histRange = { range };
	bool uniform = true, accumulate = false;
	cv::Mat y_hist;
	cv::calcHist( &image, 1, 0, cv::Mat(), y_hist, 1, &histSize, &histRange, uniform, accumulate );

	double mean_sample_value = 0;
	for( int i=0; i<histSize; i++)
	{
		mean_sample_value += y_hist.at<float>(i)*(i+1);
	}
		
	mean_sample_value /= (image.rows*image.cols);
	
	//focus_region = brightness_image[rows/2-10:rows/2+10, cols/2-10:cols/2+10]
	//brightness_value = numpy.mean(focus_region)

	float err_p = desired_msv-mean_sample_value;
	err_i += err_p;
	if( fabs(err_i) > max_i) 
	{
		err_i = (err_i>0?1:-1)*max_i;
	}

	// Start with current values
	float 	set_exposure_us = cur_exposure_ns/1000;
	*set_gain = cur_gain;

	// Process exposure with PI controller
	// Don't change exposure if we're close enough.
	// this helps prevent oscillating
	if(fabs(err_p) > p_good_thresh)
	{
		update = true;

		// Compute exposure based on PI errors
		set_exposure_us = 
			(cur_exposure_ns + k_p_ns*err_p + k_i_ns*err_i)/1000;

	}

	// Boundry check on exposure. First value from camera can be wrong
	set_exposure_us = MIN( exposure_max_us, set_exposure_us);
	set_exposure_us = MAX( exposure_min_us, set_exposure_us);
	*set_exposure_ns = set_exposure_us * 1000;

	if( update )
	{
		printf( "set exposure: %fus %luns msv: %f err_p: %f err_i: %f\n", 
			set_exposure_us, *set_exposure_ns, mean_sample_value, 
			err_p, err_i);
	}
	//
	// Calculate gain is a scaler of exposure, with a fixed offset
	// the goal is for gain to move linearly with exposure 
	// but only above a certain exposure (the offset)
	// below the exposure offset, gain is 0
	// this is to avoid digital noise when it is not needed
	int32_t exposure_offset_range = 
		exposure_max_us - exposure_offset_for_gain_calc;
	float exposure_offset_div = exposure_offset_range / 
		(gain_max - gain_min);
	*set_gain = (set_exposure_us-exposure_offset_for_gain_calc) / 
		exposure_offset_div;

	// Make sure gain doesn't go out of bounds
	*set_gain = MIN( gain_max, *set_gain);
	*set_gain = MAX( gain_min, *set_gain);

#if 1 // Debug Histogram Image
#ifndef __ANDROID__ // No highgui on Android
	if( display_debug > 0 )
	{
		int hist_w = 512, hist_h = 400;
		int bin_w = cvRound( (double) hist_w/histSize );
		cv::Mat histImage( hist_h, hist_w, CV_8UC3, cv::Scalar( 0,0,0) );
		cv::normalize(y_hist, y_hist, 0, histImage.rows, cv::NORM_MINMAX, -1, cv::Mat() );

		for( int i = 1; i < histSize; i++ )
		{
			cv::line( histImage, cv::Point( bin_w*(i-1), hist_h - cvRound(y_hist.at<float>(i-1)) ),
				cv::Point( bin_w*(i), hist_h - cvRound(y_hist.at<float>(i)) ),
				cv::Scalar( 255, 0, 0), 2, 8, 0  );
		}
		cv::imshow("calcHist Demo", histImage );
	}
#endif // __ANDROID__			
#endif


#if 1
#ifndef __ANDROID__ // No highgui on Android
	static uint64_t counter = 0;
	if( (counter&0xF) == 0 && display_debug > 0 )
	{
		// Convert to an opencv object
		cv::Mat image = cv::Mat(height, width, CV_8U, image_data).clone();

		// Show the image
		cv::imshow("Camera Image Frame", image);
		if (cv::waitKey(10) == 27)
		{
		}
	}
#endif // __ANDROID__
#endif
	return update;
}
