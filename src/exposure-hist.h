/*******************************************************************************
 * Copyright 2021 ModalAI Inc.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its contributors
 *    may be used to endorse or promote products derived from this software
 *    without specific prior written permission.
 *
 * 4. The Software is used solely in conjunction with devices provided by
 *    ModalAI Inc.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 ******************************************************************************/


#ifndef EXPOSURE_HIST_H
#define EXPOSURE_HIST_H

#include <stdint.h>


class ExposureHist
{
	public:
		ExposureHist()
		{
			// Defaults are OV7251, need per sensor configurations through config file
			gain_min = 0;
			gain_max = 1000;
			exposure_min_us = 100;
			exposure_max_us = 33000;
			desired_msv = 65.0;
			k_p_ns = 8000;
			k_i_ns = 20;
			max_i = 250;

			p_good_thresh = 5;

			exposure_period = 1;
			gain_period = 1;

			display_debug = 1;

			exposure_offset_for_gain_calc = 8000;

		}

		bool update_exposure(
			uint8_t*	image_data, 
			int 		width,
			int 		height, 
			uint64_t	cur_exposure_ns, 
			int16_t		cur_gain,
			uint64_t* 	set_exposure_ns, 
			int16_t* 	set_gain );

		// Defaults are OV7251, need per sensor configurations through config file
		uint16_t 		gain_min;
		uint16_t 		gain_max;
		uint32_t 		exposure_min_us;
		uint32_t 		exposure_max_us;

		// This is the main setpoint of the algorithm
		float 	 		desired_msv;
		// Gains
		float 			k_p_ns;
		float 			k_i_ns;
		// Maximum integral value
		float 			max_i;

		// hold-steady threashold
		unsigned int	p_good_thresh;

		unsigned int 	exposure_period;
		unsigned int 	gain_period;

		unsigned int 	display_debug;

		uint32_t		exposure_offset_for_gain_calc;
};

#endif // end #define CONFIG_FILE_H