/**
 *  @file main.cpp
 *  @brief Main file for running the application
 */
/*******************************************************************************
 * Copyright 2020 ModalAI Inc.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its contributors
 *    may be used to endorse or promote products derived from this software
 *    without specific prior written permission.
 *
 * 4. The Software is used solely in conjunction with devices provided by
 *    ModalAI Inc.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 ******************************************************************************/


// C/C++ Includes
#include <iostream>
#include <signal.h>
#include <cstring>
#include <unistd.h>
#include <stdio.h>
#include <stdlib.h>
#include <atomic>
#include <math.h>
#include <float.h>


// Package dependencies
#include "opencv2/opencv.hpp"

// ModalAI Library Includes
#include "modal_pipe.h"
#include "modal_pipe_interfaces.h"

#include "exposure-hist.h"

const std::string CLIENT_NAME = "exposure-cli";

#ifdef __ANDROID__
// this is the directory used by the vins-camera-imu-server for data  pipes
const std::string IMU_CHANNEL_DIR =  	"/data/local/tmp/vins_imu/";
const std::string CAMERA_CHANNEL_DIR =  "/data/local/tmp/vins_camera/";

#else
// this is the directory used by the vins-camera-imu-server for data  pipes
std::string CAMERA_CHANNEL_DIR =  "/tmp/vins_camera/";

#endif


// The index to use for the IMU and camera channels
static constexpr int CAMERA_CHANNEL_INDEX  	= 1;

static constexpr int CAMERA_PIPE_READ_BUF_SIZE = (sizeof(camera_image_metadata_t) + (640 * 480)) * 2;

// If We should keep running
static bool keep_running = 1;

static int16_t cur_gain = 511;

// Parameters
// TODO: use json config file for these
struct Parameters
{
	// Defaults are OV7251, need per sensor configurations through config file
	uint16_t 	gain_min = 0;
	uint16_t 	gain_max = 1000;
	uint32_t 	exposure_min_us = 100;
	uint32_t 	exposure_max_us = 33000;
	float 	 	desired_msv = 55.0;
	// Gains
	float 		k_p_ns = 80000;
	float 		k_i_ns = 200;
	// Maximum integral value
	float 		max_i = 250;

	// hold-steady threashold
	unsigned int	p_good_thresh = 5;

	unsigned int 	exposure_period = 1;
	unsigned int 	gain_period = 1;

	unsigned int 	display_debug = 0;

	uint32_t	exposure_offset_for_gain_calc = 8000;
	
};

static ExposureHist exposure;

/*
	Local Function Definitions
*/
void intHandler(int signal_value);
static void cameraDataCallback(int ch, char* data, int bytes, void* context);
void setExposureGain( uint32_t exposure_us, int16_t gain );

/** Signal Handler.  Kills the application gracefully.
 *
 *  @param[in] signal_value The signal that was issued, ignored
 */
void intHandler(int signal_value)
{
	std::cout << "Client received sigint" << std::endl;

	// Signal that we should stop running
	keep_running = false;
}

/** Data callback for the Camera.  This displays the camera data
 *
 *  @param[in] ch The channel index issuing the callback
 *  @param[in] data A pointer to the data
 *  @param[in] bytes The number of bytes in the data
 */
static void cameraDataCallback(int ch, char* data, int bytes, void* context)
{
	// Unused
	(void)ch;
	camera_image_metadata_t* metadata = NULL;
	uint8_t* pixel_bytes = NULL;
	if( bytes >= sizeof(camera_image_metadata_t) )
	{
		metadata = (camera_image_metadata_t*)data;
		pixel_bytes = (uint8_t*)&(data[sizeof(camera_image_metadata_t)]);;
		if( metadata->magic_number == CAMERA_MAGIC_NUMBER )
		{
			// References for the following algorithm
			// https://docs.opencv.org/3.4/d8/dbc/tutorial_histogram_calculation.html
			// https://github.com/alexzzhu/auto_exposure_control/blob/master/src/auto_exposure_control.py

			// TODO: Perform exposure processing
			Parameters 	p;
			
			uint64_t cur_exposure_ns = metadata->exposure_ns;
			uint64_t set_exposure_ns;
			int16_t  set_gain;

			//int16_t cur_gain = metadata->gain; // doesn't seem to be working
			printf( "received camera frame - exposure %lu gain %d (recv-%d)\n", 
				cur_exposure_ns, cur_gain, metadata->gain);

			bool update = exposure.update_exposure(
				pixel_bytes,
				metadata->width,
				metadata->height,
				cur_exposure_ns,
				cur_gain,
				&set_exposure_ns,
				&set_gain
			);

			if( update )
			{
				// Make the call to set gain and exposure
				setExposureGain( set_exposure_ns/1000, set_gain );
				cur_gain = set_gain;
			}
		}
		else
		{
			std::cout << "Invalid magic number: " << metadata->magic_number << std::endl;
		}

	}
	else
	{
		std::cout << "Invalid packet side set_camera_exposure_gain_packet_t: " << bytes << std::endl;
	}

	return;
}

void setExposureGain( uint32_t exposure_us, int16_t gain )
{
	printf("sending command exposure: %d gain %d\n", exposure_us, gain);
	set_camera_exposure_gain_packet_t exposure;
	exposure.magic_number = CAMERA_MAGIC_NUMBER;
	exposure.exposure_ns = exposure_us*1000;
	exposure.gain = gain;
	pipe_client_send_control_cmd_bytes(
		CAMERA_CHANNEL_INDEX, 
		(char*)&exposure, 
		sizeof(set_camera_exposure_gain_packet_t) );
	

}


/** Main Function
 *
 *  @param[in] argc Argument count
 *  @param[in] argv Arguments
 */
int main(int argc, char **argv)
{
	printf( "Welcome to voxl-mpa-exposure\n");

	if( argc > 1 )
	{
		CAMERA_CHANNEL_DIR = std::string( argv[1] );
	}

	// Register the signal so we can end the application
	signal(SIGINT, intHandler);

	// request a new pipe from the server and assign callback
	if (pipe_client_init_channel(CAMERA_CHANNEL_INDEX,
	                             const_cast<char*>(CAMERA_CHANNEL_DIR.c_str()),
	                             const_cast<char*>(CLIENT_NAME.c_str()),
	                             1,
	                             CAMERA_PIPE_READ_BUF_SIZE))
	{
		std::cerr << "Client could not init Camera channel" << std::endl;
		return -1;
	}
	pipe_client_set_simple_helper_cb(CAMERA_CHANNEL_INDEX, cameraDataCallback, NULL);

	// keep going until signal handler sets the running flag to 0
	while (keep_running)
	{
		usleep(100 * 1000);
	}

	// all done, signal pipe read threads to stop
	pipe_client_close_all();
	std::cout << "Client exited cleanly" << std::endl;

	return 0;
}
